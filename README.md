# Math Entity Card Prototype

Creates a Math Entity Card from static response data. There exists a _carousel_ like feature for multiple descriptions and a _more_ button for added examples.

This prototype is then modified by Gavin Nishizawa and used as part of MathSeer.

Modifications Include

1.  Removal of the Carousel Feature.

2.  Using pre-rendered SVG images of Formulas

3. The chip for formulas behave as a chip

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
The server starts up on 8080 unless the port is already in used.

Navigate to  localhost:8080/entity  in your browser or to localhost:8080 and follow the first two steps.

1. Click on the navigation bar in the top left corner and select Entity Card.

2. The page should now reload with 4 Math Entity Cards
    * Factorial with Title, Formula and Definition.  
    On clicking the MORE button a collapsable menu containing hyperlinks to 'Gamma Function', 'Rising Factorial', 'Falling Factorial' are displayed.   Source that links to corresponding  source (Wiktionary).
    * Pythagorean Equation with Title, Formula and Definition.
    * Wiener-Hopf Method with Title, Formula only.
    * Pythagorean Theorem with formula, *About* tab and a carousel with rotating descriptions on click.


### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

## Built With

* [Vuetify](https://vuetifyjs.com/en/) 

## Authors

* **Abishai Dmello** - *Initial work* 

* **Gavin Nishizawa** - *Modified Prototype for MathSeer* 

## License

This project is licensed under the GNU GPLv3  License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Net Ninja for the Vue and Vuetifuy tutorial